#!/bin/bash
set -euxo pipefail
# Este script se encarga de clonar los microservicios y copiar las credenciales de firebase en cada directorio

echo 'Clonando API services:'
echo 'Service 1.'
rm -rf microservice1
git clone https://gitlab.com/i.arzaut/microservice1.git
cd microservice1
cp ../../env/simple-comercio-firebase-admin.json .
cp ../../env/postgres-ca-cert.crt config
cd ..
echo '------------------------------------------------------------------------------'

echo 'Service 2'
rm -rf microservice2
git clone https://gitlab.com/i.arzaut/microservice2.git
cd microservice2
cp ../../env/simple-comercio-firebase-admin.json .
cd ..
echo '------------------------------------------------------------------------------'
