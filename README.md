### Pasos a seguir:
1. Dentro del directorio **'/env'**, quitarle la extension '.example' a los archivos de variables de entorno y completarlos con informacion valida **(esto incluye los archivos env.service1, env.service2, postgres-ca-cert y simple-comercio-firebase-admin)**
2. Dentro del directorio **'/backend'**, correr el script ```clone_backend.sh``` para clonar los microservicios
3. Correr el comando ```docker compose up -d```
